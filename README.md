## Description of the codes repository

---

This directory contains list the following sub-directories:

**/coursera**
	
	[MATLAB] my implementation files from Andrew Ng's online coursera course

**/pythons**
	
	[PYTHON] selected python codes

**/thesis**
	
	[PYTHON] codes from my MSC thesis showing how h2O framework is used carry out deep learning auto-encoder and neural nets with python client