### Multi layer configuration:  ###
# # Activation fxn: Rectifier
# # Hidden layer(s): 3
# # Hidden config: [10,10,10]
# # No. of epochs: 10000

import h2o
from h2o.estimators.deeplearning import H2ODeepLearningEstimator


# In[3]:


h2o.init()


# In[8]:


from h2o.utils.shared_utils import _locate # private function. used to find files within h2o git project directory.

data = h2o.upload_file("credit_fraud.arff")
data.describe()


# In[36]:


data["class"] = data["class"].asfactor()
model = H2ODeepLearningEstimator(activation = "Rectifier", hidden = [10,10,10], epochs = 10000)
model.train(x = list(set(data.columns) - set(["class"])), y ="class", training_frame = data)
model.show()


# In[37]:


predictions = model.predict(data)
predictions.show()


# In[38]:


performance = model.model_performance(data)
performance.show()

