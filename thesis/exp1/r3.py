### Multi layer configuration:  ###
# # Activation fxn: Rectifier
# # Hidden layer(s): 2
# # Hidden config: [10,10]
# # No. of epochs: 10

import h2o
from h2o.estimators.deeplearning import H2ODeepLearningEstimator


# In[5]:


h2o.init()


# In[17]:


train = h2o.upload_file("credit_fraud_train.arff")
test = h2o.upload_file("credit_fraud_test.arff")

predictors = list(range(0,20))
resp = 20

#train = train[predictors]
#test = test[predictors]


# In[54]:


#data["class"] = data["class"].asfactor()
model = H2ODeepLearningEstimator(activation = "Rectifier", hidden = [10,10], epochs = 10)
model.train(x = predictors, y = resp, training_frame = train)
model.show()


# In[55]:


predictions = model.predict(test)
predictions.show()


# In[56]:


performance = model.model_performance(test)
performance.show()

