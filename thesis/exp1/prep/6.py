
# coding: utf-8

# In[1]:


import h2o
from h2o.estimators.deeplearning import H2ODeepLearningEstimator


# In[2]:


h2o.init()


# In[3]:


train = h2o.upload_file("credit_fraud_train.arff")
test = h2o.upload_file("credit_fraud_test.arff")

predictors = list(range(0,20))
resp = 20

#train = train[predictors]
#test = test[predictors]


# In[11]:


#data["class"] = data["class"].asfactor()
model = H2ODeepLearningEstimator(activation = "Tanh", hidden = [10], epochs = 1000)
model.train(x = predictors, y = resp, training_frame = train)
model.show()


# In[7]:


predictions = model.predict(test)
predictions.show()


# In[12]:


performance = model.model_performance(test)
performance.show()

