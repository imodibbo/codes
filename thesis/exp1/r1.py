### Multi layer configuration:  ###
# # Activation fxn: Rectifier
# # Hidden layer(s): 1
# # Hidden config: [10]
# # No. of epochs: 1

import h2o
from h2o.estimators.deeplearning import H2ODeepLearningEstimator


# In[5]:


h2o.init()


# In[17]:


train = h2o.upload_file("credit_fraud_train.arff")
test = h2o.upload_file("credit_fraud_test.arff")

predictors = list(range(0,20))
resp = 20

#train = train[predictors]
#test = test[predictors]


# In[30]:


#data["class"] = data["class"].asfactor()
model = H2ODeepLearningEstimator(activation = "Rectifier", hidden = [10], epochs = 1)
model.train(x = predictors, y = resp, training_frame = train)
model.show()


# In[33]:


predictions = model.predict(test)
predictions.show()


# In[34]:


performance = model.model_performance(test)
performance.show()

