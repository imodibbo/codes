### Multi layer configuration:  ###
# # Activation fxn: Maxout
# # Hidden layer(s): 1
# # Hidden config: [10]
# # No. of epochs: 10

import h2o
from h2o.estimators.deeplearning import H2ODeepLearningEstimator


# In[2]:


h2o.init()


# In[3]:


train = h2o.upload_file("credit_fraud_train.arff")
test = h2o.upload_file("credit_fraud_test.arff")

predictors = list(range(0,20))
resp = 20

#train = train[predictors]
#test = test[predictors]


# In[4]:


#data["class"] = data["class"].asfactor()
model = H2ODeepLearningEstimator(activation = "Maxout", hidden = [10], epochs = 10)
model.train(x = predictors, y = resp, training_frame = train)
model.show()


# In[5]:


predictions = model.predict(test)
predictions.show()


# In[6]:


performance = model.model_performance(test)
performance.show()

