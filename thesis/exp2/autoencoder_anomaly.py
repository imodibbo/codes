### Deep learning autoencoder configuration:  ###
# # Activation fxn: Tanh
# # Hidden config: [50]
# # No. of epochs: 1000


from __future__ import print_function
from builtins import range
import sys, os
import h2o
from h2o.estimators.deeplearning import H2OAutoEncoderEstimator


# In[42]:

h2o.init()


# In[61]:

train = h2o.upload_file("credit_fraud_train.arff")
test = h2o.upload_file("credit_fraud_test.arff")

predictors = list(range(0,21))
resp = 21
train = train[predictors]
test = test[predictors]


# In[13]:

# train unsupervised Deep Learning autoencoder model on train_hex

ae_model = H2OAutoEncoderEstimator(activation="Tanh", hidden=[50], ignore_const_cols=False, epochs=1000)
ae_model.train(x=predictors,training_frame=train)


# In[14]:

test_rec_error = ae_model.anomaly(test)

test_rec_error.describe()


# In[ ]:



