## Contents Description

---

This directory contains code implementation from my M.Sc. thesis. The arrangement of subdirs is described below:

1. The report **thesis.pdf** contains detailed description of the research experiment conducted, the arrangement of codes are as below:
2. Multilayer Feedforward Network is described on *page 20* and implemented as in **/exp1**; while the autoencoder anomaly detector is described on *page 21* and implemented as in **/exp2**.