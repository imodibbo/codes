import random
import math

# sum 1 to 100
def sum100(): # no argument
    ret = 0
    for i in range(1,101):
        ret = ret + i
    return ret

# sum 1 to n
def sumn(n): # 1 argument
    ret = 0
    for i in range(1,n+1):
        ret = ret + i
    return ret

# print(sum100())
# print 'Sum 1 to 100 is', sumn(100)

# function to choose random pair groups
def randomGrpPair():
    group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    first = random.randrange(0,len(group))
    second = random.randrange(0,len(group))
    return 'Random pairs: Groups('+group[first]+','+group[second]+')'

#print randomGrpPair()

# function to calculate
def hypoth(o,a):
    return math.sqrt(o*o+a*a)

# print hypoth(7,8)

#function to calculate mean of an array
def meanArr(arr):
    total = 0.0
    for num in arr:
        total = total + num
    return total/len(arr)

#print meanArr([2, 5, 6, 8, 3, 1, 3])

group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

def swap(arr,lft,ryt):
    tmp = arr[lft]
    arr[lft] = arr[ryt]
    arr[ryt] = tmp
    
def shuffleArr(arr):
    sz = len(arr)
    for ind in range(sz):
        rnd = random.randrange(ind,sz)
        swap(arr, ind, rnd)
    print arr

# shuffleArr(group)
# print group
