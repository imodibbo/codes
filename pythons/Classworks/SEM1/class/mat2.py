import sys
import dotpro

mat = [] # matrix of grades
un = []   # vector of credits
cgpa = []

def sumi(vec):
    tt = 0
    for v in vec:
        tt += int(v)
    return tt

n = int(input("Number of students: "))

# input mat of grades
print "Enter Matrix of grades> "
for i in range(n):
    li = sys.stdin.readline().strip('\n')
    li2 = sys.stdin.readline().strip('\n')
    mat.append([li.split(' '),li2.split(' ')])


# compute cgpa of studs
for std in mat:
    cgpa.append(dotpro.dotpro(std[0],std[1])/sumi(std[1]))

print cgpa
