# function that calculates dot product of
# vectors u and v

def dotpro(u,v):
    if len(u) != len(v):
        print("Vectors are of different length")
        return

    # initialise result to 0
    res = 0
    for i in range(len(u)):
	res += int(u[i])*int(v[i])
    return res
