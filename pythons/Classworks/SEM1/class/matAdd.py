# a = 1 2 3
#      4 5 6

# b = -6 -5 -4
#       -3 -2 -1

a = [[1,2,3],[4,5,6]] 
b = [[-6,-5,-4],[-3,-2,-1]]

# c = a + b

# to compute c, initialise to 0
c = [[0,0,0],[0,0,0]]
# or more generally:
# c = []
# for i in range(rows):
#    r = [0] * cols
#    c += [r]

for row in range(2):
    for col in range(3):
        c[row][col] = a[row][col] + b[row][col]

print c
