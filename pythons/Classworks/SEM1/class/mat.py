import sys
import dotpro

mat = [] # matrix of grades
un = []   # vector of credits
cgpa = []

n = int(input("Number of students: "))

# input mat of grades
print "Enter Matrix of grades> "
for i in range(n):
    li = sys.stdin.readline().strip('\n')
    mat += [li.split(' ')]

# input vec of credits
print  "Enter Vec of credit units> "
un += sys.stdin.readline().strip('\n').split(' ')

tt = 0
for v in un:
    tt += int(v)
#tt = sum(un) # memoise total credit units

# compute cgpa of studs
for std in mat:
    cgpa.append(dotpro.dotpro(std,un)/tt)

print cgpa
