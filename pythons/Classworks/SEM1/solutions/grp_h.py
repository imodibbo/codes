## program that reads in text and reports count of words 
## in the text

## --need to make it case insensitive, that's all

import sys

# str1 = sys.stdin.read() 
sep = []

# for line in sys.stdin:
#    sep += line.split(' ')
while True:
    line = sys.stdin.readline()
    if line == '':
        break
    sep += line.replace('\n','').split(' ')
    
clean = [item for item in sep if item!='']

for item in sorted(set(clean)):
    print item,clean.count(item)
    
# print(clean)
