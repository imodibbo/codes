def q2():
    members = ['Sharon','Ibrahim','Abdullahi','Badiyya']
    for second in sorted(members):
        for first in reversed(members):
            print(first,second)

def q3():
    a = ['A','B','B','C','C','C','D','E','F','G','G','H','H','H','I']
    b = range(1,2017)
    c = range(1,10)

    ourdict = {b[i]:a[i] for i in range(len(a))}

    for x,y in ourdict.items():
        if x in c:
            print(str(y)+str(x))

def q5():
    pl = ['P','Y','T','H','O','N','3']
    
    l1 = [3,5,4,4,5,6]
    l2 = [6,6,6,6,6,6]

    test = ['C','S','C',' ','2','2','0']
    t_vs = [4,3,4,5,6,6]

##    shuffleArr(pl,l1)
##    print()
##    shuffleArr(pl,l2)
    shuffleArr(test,t_vs)

def swap(arr,lft,ryt):
    tmp = arr[lft]
    arr[lft] = arr[ryt]
    arr[ryt] = tmp
    
def shuffleArr(arr,v):
    sz = len(arr)
    for ind in range(sz-1):        
        print('(Itr \#'+str(ind+1)+': i='+str(ind)+', r='+str(v[ind])+', $course['+str(ind)+']$ swaps $course['+str(v[ind])+']$ $\Rightarrow$)')
        swap(arr, ind, v[ind])
        print(arr)

##print('Question 2')
##print()
##q2()
##print()
##print('Question 3')
##print()
##q3()
##print()
print('Question 5')
print()
q5()
