import random

def randomPair():
    group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    rand1  = random.randrange(0,len(group))
    rand2  = random.randrange(0,len(group))

    return 'Group pair: ('+group[rand1]+','+group[rand2]+')'

# presentation order: C,G,B,F,D,E,A,H

print randomPair()
