# program that does reads two matrices from 'stdin' (multi-line input)
# and does an explicit addition of the matrices. This is a complete
# solution.

import sys # library to read from stdin

# Read mat 1
print "Enter Mat 1> (end with \eof char)"
read1 = sys.stdin.read()

# Read mat 2
print "Enter Mat 2> (end with \eof char)"
read2 = sys.stdin.read()

# mat 1 and 2 are strings with \n separating lines
# e.g. mat = '2 3 4\n1 2 3\n' for a 2by3 matrix

# initialise matrices
mat1 = []
mat2 = []

# to clean and convert to a list of Ints:
# 1. remove the trailing \n char using strip('\n'), pop(), etc
# 2. break to list of rows at \n points and to list of chars at ' ' points
# using .split('\n') and split(' ')
# 3. convert list of Chars to Ints

# 1.
read1 = read1.strip()
read2 = read2.strip()

# 2. & 3.
for r1,r2 in zip(read1.split('\n'),read2.split('\n')):
	mat1.append([int(x) for x in r1.split(' ')])
	mat2.append([int(x) for x in r2.split(' ')])

# for debugging	
assert len(mat1) == len(mat2)

# assume res = mat1+mat2
res = []
t = [] # a tmp buffer

# the matrix addition
for row in range(len(mat1)): # iterate through no. of rows
		for col in range(len(mat1[0])): # iterate through no. of cols
			s = mat1[row][col]+mat2[row][col] # sum corresponding elems
			t.append(s) # add to a temp storage, represents a row			
		res.append(t) # add full row to 'result' list
		t = [] # clear t for next iteration

print 'The sum of inputed matrices is:'

for line in res:
	print(line)