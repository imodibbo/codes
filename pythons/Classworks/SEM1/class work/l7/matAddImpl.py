# program that does 'explicit' addition of two matrices
# this is easy to code but is limited in capability: only works if you know
# matrices sizes in advance

# first matrix
# a = 1 2 3
#      4 5 6

# second matrix
# b = -6 -5 -4
#       -3 -2 -1

# python representation
a = [[1,2,3],[4,5,6]] 
b = [[-6,-5,-4],[-3,-2,-1]]

# let c = a + b

# to compute c, initialise to 0
c = [[0,0,0],[0,0,0]]  # explicit initialisation

# or more generally:
# c = []
# for i in range(rows):
#    r = [0] * cols
#    c += [r]

for row in range(2): # iterate number of rows
    for col in range(3): # iterate number of cols
        c[row][col] = a[row][col] + b[row][col]  # **explicit** assignment 

print c
