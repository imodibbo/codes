import sys

def med3():
    chars = sys.stdin.readline().strip().split(' ')
    num = [int(x) for x in chars]
    max3 = max(num)
    min3 = min(num)

    med = sum(num)-min3-max3
    print med

med3()
