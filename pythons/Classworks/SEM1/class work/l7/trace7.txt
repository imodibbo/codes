Python 3.5.1 (v3.5.1:37a07cee5969, Dec  6 2015, 01:54:25) [MSC v.1900 64 bit (AMD64)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> str1 = 'CSC'
>>> str2 = '220'
>>> str1
'CSC'
>>> str1 - str2
Traceback (most recent call last):
  File "<pyshell#3>", line 1, in <module>
    str1 - str2
TypeError: unsupported operand type(s) for -: 'str' and 'str'
>>> str1*str2
Traceback (most recent call last):
  File "<pyshell#4>", line 1, in <module>
    str1*str2
TypeError: can't multiply sequence by non-int of type 'str'
>>> str1+str2
'CSC220'
>>> str1[0]
'C'
>>> str2[2]
'0'
>>> str1[-2]
'S'
>>> str2 = 'You can take numbers'
>>> str2[6]
'n'
>>> str2[9]
'a'
>>> str2[8]
't'
>>> str2[6:10]
'n ta'
>>> str[8:12]
Traceback (most recent call last):
  File "<pyshell#14>", line 1, in <module>
    str[8:12]
TypeError: 'type' object is not subscriptable
>>> str2[8:12]
'take'
>>> str2[8]='T'
Traceback (most recent call last):
  File "<pyshell#16>", line 1, in <module>
    str2[8]='T'
TypeError: 'str' object does not support item assignment
>>> len(str2)
20
>>> groups = ['A', 'B']
>>> groups
['A', 'B']
>>> groups + ['C']
['A', 'B', 'C']
>>> groups
['A', 'B']
>>> groups += 23
Traceback (most recent call last):
  File "<pyshell#22>", line 1, in <module>
    groups += 23
TypeError: 'int' object is not iterable
>>> groups += '23'
>>> groups
['A', 'B', '2', '3']
>>> groups += ['23']
>>> groups
['A', 'B', '2', '3', '23']
>>> groups[2:] = ['D', 'E', 'F', 'G', 'H']
>>> groups
['A', 'B', 'D', 'E', 'F', 'G', 'H']
>>> actors = ['Farmer', 'Goat', 'Lion', 'Yam']
>>> actors
['Farmer', 'Goat', 'Lion', 'Yam']
>>> actors[0:1]
['Farmer']
>>> actors[0:2]
['Farmer', 'Goat']
>>> for actor in actors:
	for partner in actors:
		if actor != partner:
			print(actor,partner)

Farmer Goat
Farmer Lion
Farmer Yam
Goat Farmer
Goat Lion
Goat Yam
Lion Farmer
Lion Goat
Lion Yam
Yam Farmer
Yam Goat
Yam Lion
>>> copy = actors
>>> copy += ['Imran']
>>> copy
['Farmer', 'Goat', 'Lion', 'Yam', 'Imran']
>>> copy[0] = copy[4]
>>> copy
['Imran', 'Goat', 'Lion', 'Yam', 'Imran']
>>> copy.pop()
'Imran'
>>> copy
['Imran', 'Goat', 'Lion', 'Yam']
>>> actors
['Imran', 'Goat', 'Lion', 'Yam']
>>> num1 = 100
>>> num2 = num1
>>> num2
100
>>> num2 = num2/10
>>> num2
10.0
>>> num1
100
>>> copy = actors[:]
>>> copy
['Imran', 'Goat', 'Lion', 'Yam']
>>> copy.pop()
'Yam'
>>> copy
['Imran', 'Goat', 'Lion']
>>> actors
['Imran', 'Goat', 'Lion', 'Yam']
>>> std1 = [5,2,4,1,5]
>>> std2= [3,4,5,2,5]
>>> std3 = [5,5,5,3,5]
>>> std4 = [2,1,2,4,5]
>>> std5 = [5,3,2,5,5]
>>> std4[0]
2
>>> stdGrad = [ std1, std2, std3, std4, std5 ]
>>> stdGrad
[[5, 2, 4, 1, 5], [3, 4, 5, 2, 5], [5, 5, 5, 3, 5], [2, 1, 2, 4, 5], [5, 3, 2, 5, 5]]
>>> stdGrad[2][3]
3
>>> stdGrad[1][3]
2
>>> stdGrad[1][3] = 4
>>> stdGrad[1][3]
4
>>> arr1 = [[1,3],[2,4]]
>>> arr2 = [[-1,-2],[-3,-4]]
>>> arr1+arr2
[[1, 3], [2, 4], [-1, -2], [-3, -4]]
>>> res = []
>>> for row in range(2):
	for col in range(2):
		res += arr1[row][col] + arr2[row][col]

Traceback (most recent call last):
  File "<pyshell#80>", line 3, in <module>
    res += arr1[row][col] + arr2[row][col]
TypeError: 'int' object is not iterable
>>> range(2)
range(0, 2)
>>> for row in range(0,2):
	for col in range(0,2):
		res += arr1[row][col] + arr2[row][col]

Traceback (most recent call last):
  File "<pyshell#83>", line 3, in <module>
    res += arr1[row][col] + arr2[row][col]
TypeError: 'int' object is not iterable
>>> range(0,2)
range(0, 2)
>>> for row in [0,1,2]:
	for col in [0,1,2]:
		res += arr1[row][col] + arr2[row][col]

Traceback (most recent call last):
  File "<pyshell#86>", line 3, in <module>
    res += arr1[row][col] + arr2[row][col]
TypeError: 'int' object is not iterable
>>> for row in [0,1,2]:
	for col in [0,1,2]:
		res += [arr1[row][col] + arr2[row][col]]

Traceback (most recent call last):
  File "<pyshell#88>", line 3, in <module>
    res += [arr1[row][col] + arr2[row][col]]
IndexError: list index out of range
>>> for i in range(2): # iterate through no. of rows
		for j in range(2): # iterate through no. of cols
			t1 = arr1[i][j]+arr2[i][j] # sum corresponding elems
			t.append(t1) # add to a temp storage, represents a row
			t1 = []	# clear t1 for next iteration
		res.append(t) # add full row to 'result' list
		t = [] # clear t for next iteration
	
>>> res
[[0, 1], [-1, 0]]