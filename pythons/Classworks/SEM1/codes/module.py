import random

def meanArr(arr):
    return sum(arr)//len(arr)

def randomPair():
    group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    rand1  = random.randrange(0,len(group))
    rand2  = random.randrange(0,len(group))

    return 'Group pair: ('+group[rand1]+','+group[rand2]+')'

def sumn(n):
    sumn = 0
    for k in range(1,n+1):
        sumn = sumn + k
    return sumn

