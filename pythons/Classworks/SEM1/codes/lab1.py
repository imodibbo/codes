### CODES FROM LAB 1 FOR PRINT ###

#####################
#- meanArr.py
def meanArr(arr):
    return sum(arr)//len(arr)

print meanArr([2,4,5,1,7,9,2,6])


#####################
#- randomPair.py
import random

def randomPair():
    group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    rand1  = random.randrange(0,len(group))
    rand2  = random.randrange(0,len(group))

    return 'Group pair: ('+group[rand1]+','+group[rand2]+')'

# presentation order: C,G,B,F,D,E,A,H

print randomPair()


#####################
#- sumn.py
def sumn(n):
    sumn = 0
    for k in range(1,n+1):
        sumn = sumn + k
    return sumn

print sumn(38934343)


#####################
#- sum100.py
def sum100():
    sum100 = 0
    for k in range(1,101):
        sum100 = sum100 + k
    return sum100

print sum100()
        

#####################
#- module.py
import random

def meanArr(arr):
    return sum(arr)//len(arr)

def randomPair():
    group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    rand1  = random.randrange(0,len(group))
    rand2  = random.randrange(0,len(group))

    return 'Group pair: ('+group[rand1]+','+group[rand2]+')'

def sumn(n):
    sumn = 0
    for k in range(1,n+1):
        sumn = sumn + k
    return sumn


#####################
#- cliet.py
import module

print module.meanArr([2,4,5,1,7,9,2,6])


#####################
#- more.py
import random
import math

# function to calculate hypothenuse
def hypoth(o,a):
    return math.sqrt(o*o+a*a)

# print hypoth(7,8)

#function to calculate mean of an array
def meanArr(arr):
    total = 0.0
    for num in arr:
        total = total + num
    return total/len(arr)

#print meanArr([2, 5, 6, 8, 3, 1, 3])

#function to shuffle a list of elements
group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

def swap(arr,lft,ryt):   # a helper function that supports shuffleArr()
    tmp = arr[lft]
    arr[lft] = arr[ryt]
    arr[ryt] = tmp
    
def shuffleArr(arr):
    sz = len(arr)
    for ind in range(sz):
        rnd = random.randrange(ind,sz)
        swap(arr, ind, rnd)
    print arr

# shuffleArr(group)
# print group