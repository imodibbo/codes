import random
import math

# function to calculate hypothenuse
def hypoth(o,a):
    return math.sqrt(o*o+a*a)

# print hypoth(7,8)

#function to calculate mean of an array
def meanArr(arr):
    total = 0.0
    for num in arr:
        total = total + num
    return total/len(arr)

#print meanArr([2, 5, 6, 8, 3, 1, 3])

#function to shuffle a list of elements
group = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

def swap(arr,lft,ryt):   # a helper function that supports shuffleArr()
    tmp = arr[lft]
    arr[lft] = arr[ryt]
    arr[ryt] = tmp
    
def shuffleArr(arr):
    sz = len(arr)
    for ind in range(sz):
        rnd = random.randrange(ind,sz)
        swap(arr, ind, rnd)
    print arr

# shuffleArr(group)
# print group
