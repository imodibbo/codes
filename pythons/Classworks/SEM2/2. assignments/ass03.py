# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 19:04:20 2018

@author: HacKafe
"""
#
# ASS03B
#
#PM=float(input("PM: "))
#p=float(input("Save %: "))
#C=float(input("Cost: "))
#si=float(input("Sem In: "))
#
#sa=0.0 # sa=save
#mo=0 # mo=months
#
#for i in range(1,1000):
#    if(sa>=C):
#        break;
#    mo=mo+1
#    sa+=p*PM
#    
#    if(i%6==0):
#        PM=PM*(1+si)
#print(mo)



#
#ASS03C
#
PM=float(input("PM: "))
C=500000.0
si=0.07
mos=25

def countmos(n,PM): # doesn't count months anymore, 
                 # returns accumulated save within months range
    sa=0.0 # sa=save
    mos=25 # no-months
    
    for i in range(1,100000):
        if(mos<1):
            break;
        mos=mos-1
        sa+=n*PM
        
        if(i%6==0):
            PM=PM*(1+si)
            
    return sa

co=0 # bisection count
left=0 
right=10000

while(left<right):
    co=co+1
    p=left+(right-left)//2
    am=countmos(p/10000.0,PM)
    print("iter=",co,"p=",p,"am=",am)
    if(abs(am-C)<100):
        print("Best saving rate:",p/10000.0)
        print("Steps in BS:",co)
        break
    elif(am<C):
        left=p
    else:
        right=p

else:
    print("IMPOSSIBLE")
