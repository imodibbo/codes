# SOLUTION TO QUESTION 5, CSC220 2017/2018 EXAMINATION
import math

### N is prime iff none of numbers 2 to sqrt(N) divides N

# recursion driver function
def isPrime(N):
    sq = math.sqrt(N)
    return recurIsPrime(2,int(sq),N) 

def recurIsPrime(i,sq,N):
    if i==sq:
        return True
    if N%i==0:
        return False
    return recurIsPrime(i+1,sq,N)
