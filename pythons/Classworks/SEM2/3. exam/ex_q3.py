# SOLUTION TO QUESTION 3, CSC220 2017/2018 EXAMINATION
N = int(input('Enter the value of N:' ))
for csc220 in range(abs(N)+1):
    if csc220**3 >= abs(N):
        break       
if csc220**3 != abs(N):
    print(N, 'has failed')
else:
    if N < 0:
        csc220 = -csc220
    print(N, 'has passed with score',csc220)
