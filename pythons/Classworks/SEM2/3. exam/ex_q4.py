# SOLUTION TO QUESTION 4, CSC220 2017/2018 EXAMINATION
def  computeGPAs(gr):
    tga = {}
    tca = {}
    for course in gr:
        for std,let in course.items():
            if let == 'A':
                v = 5
            elif let == 'B':
                v = 4
            elif let == 'C':
                v = 3
            elif let == 'D':
                v = 2
            else:
                v = 0
            if std not in tga:
                tga[std]=v
                tca[std]=1
            else:
                tga[std]+=v
                tca[std]+=1
    gp = {s:tga[s]/tca[s] for s in tga.keys()}
    return gp
