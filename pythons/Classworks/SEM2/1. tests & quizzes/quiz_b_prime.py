import math

### N is prime iff none of numbers 2 to sqrt(N) divides N

# a) recurIsPrime() that takes positive int N, uses the rule
#    described above and returns booleans as accordingly
def recurIsPrime(N):
	sq = math.sqrt(N)
	return isPrime(2,int(sq),N) 

# main recursion driver
def isPrime(i,sq,N):
	if i==sq:
		return True
	if N%i==0:
		return False
	return isPrime(i+1,sq,N)

# b) recurPrimes100() that uses recurIsPrime() above to 
#    return all prime numbers less than 100
def recurPrimes100():
    bound = 100
    t = ()
    
    primes100(2,bound,t)
    
    return t

# main recursion driver
def primes100(n,b,li):
    if n > b:
        return
    if recurIsPrime(n):
        li = li + (n,)
    primes100(n+1,b,li)