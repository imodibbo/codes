import math
	
def cubeRt(N):
    eps = 0.01				# for 2 d.p. error check
    cubeRt = 0.0			# first cubeRt "guess"		
    step = 0.01				# increment value
    best = math.inf	# best cubeRt so far starting with dum-
							# my infinity value
	
    while abs(cubeRt**3-N) >= eps and cubeRt <= N:
        cubeRt = cubeRt + step
        diff = abs(cubeRt**3-N)

        if diff < abs(best**3-N):
            best = cubeRt
            
    return best
