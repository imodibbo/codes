# problems Ch2
# intro to algorithms by Cormen et al. 

# 2-4
# a. indices pairs 1,5;2,5;3,4;3,5;4,5
# b. the array {n,n-1,...,3,2,1} with total invs = (n-1)+(n-2)+...+2+1 = n(n-1)/2
# c. for every insertion step(move x from index j to i, i<j), all inversions are 'corrected'
#     in the index region [1-j](as x move down to i,reducing an inversion at a time), and
#     this corrected region is never 'disrupted' => running time of insertion 
#     sort is exactly no. of inversions
# d. (adapted from merge sort)
# count happens at points of merge, any two comparisons that flags are added, this
# shown as mergeInv (below). recursive merge routine, invCount -> 2T(n/2), mergeInv->O(n)
#                                                                                           =>O(nlgn)
#
mergeInv(a,p,q,r):
  A = a[p:q],A+=inf
  B = a[q+1:r],B+=inf
  i=j=c=0
  for k in [p:r]
      s = 0
      if A[i] >= B[j]
          j++,s++
      else:
          i++
      c += s*(len(A)-i)  # each 's' of x at A[i] counts in all elems indices A[i+1,end]
  return c

invCount(a,p,r):
  m = p+(p-r)//2
  return mergeInv(a,p,m)+mergeInv(a,m+1,r)
