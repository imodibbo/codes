# exercises Ch2

#2.3-7
#1. sort set of ints using mergesort -> O(nlgn)
#2. iterate sorted list and for each 'a' at index i, search rest of list, from index i+1 to end, for b=x-a using BS -> nXlgn -> O(nlgn)

#2.3-5
def BS(a,k):
    i = binSearch(a,k,0,len(a))
    print(i)

def binSearch(a,k,l,r):
    m = l+(r-l)//2
    if l==r: # recursion base case
        return -1
    elif k==a[m]:
        return m
    elif k<a[m]:
        return binSearch(a,k,l,m)
    else:
        return binSearch(a,k,m+1,r)

#2.3-2
def merge2(a,p,q,r):
    A=[]
    B=[]
    
    for v in range(p,q+1):
        A.append(a[v])
        
    for v in range(q+1,r+1):
        B.append(a[v])
       
    i=j=0
    end=False
    for k in range(p,r+1):
        if A[i] <= B[j]:
            a[k]=A[i]            
            if i==len(A)-1: # reached end of A
                for s in range(j,len(B)): # copy rest of B
                    k = k+1
                    a[k]=B[s]                    
                end=True
            i = i+1
        else:
            a[k]=B[j]            
            if j==len(B)-1: # reached end of B
                for s in range(i,len(A)): # copy rest of A
                    k=k+1
                    a[k]=A[s]                    
                end=True
            j = j+1
            
        if end:
            print(a)
            break
