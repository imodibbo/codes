# merge sort

# the merge routine
def merge(a,p,q,r):
    A=[]
    B=[]
    
    for v in range(p,q+1):
        A.append(a[v])
    A.append(math.inf)
    #print(A)
    
    for v in range(q+1,r+1):
        B.append(a[v])
    B.append(math.inf)
    #print(B)
    
    i=j=0
    for k in range(p,r+1):
        if A[i] <= B[j]:
            a[k]=A[i]
            i = i+1
        else:
            a[k]=B[j]
            j = j+1
    # print(a)
    
# merge sort routine
def mergeSort(a,p,r):
    if p<r:
        q = (p+r)//2
        mergeSort(a,p,q)
        mergeSort(a,q+1,r)
        merge(a,p,q,r)

# initial call to merge sort routine    
def MS(a):
    mergeSort(a,0,len(a)-1)
    print(a)
