# selection sort
def selectSort(a):
    k=0 # index of current smallest
    for i in range(len(a)-1):
        s = a[i+1]
        k = i+1
        for j in range(i+2,len(a)):
            if a[j]<s:
                s=a[j]
                k=j
        a[i],a[k]=a[k],a[i]
    print(a)
