## Contents Description

---

This subdirectory contains some of my python codes.

**/Classworks** 
    
    -Contains some python scripts from the beginner programming course I taught this year 2018 (SEM2) and last year 2017 (SEM1).

    -Most of it is either **Assignment solutions** or **test implementation solutions.**

**/Solutions-Cormen** 
	
    contains some of my old implementations I found from my readings of Introduction to Algorithm by Cormen et al.